# Maggic

Maggic (Magic Guile Commit) is an auto deploy commit in GNU Guile.

## Requeriments
* guile
* lsh or ssh


## Usage
### Commit
```
maggic -c user@host 'code'
```

### Push
```
maggic -p user@host file
```

### Version
```
maggic -v
```

### Help
```
maggic -h
```

## Note
* The arguments for commit must have separeted by '&&' or ';' without quotes;
* The commit must have simple quotes between code;
* The push script file must have one command per line
* LSH support in development.