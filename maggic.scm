#!/bin/sh
# -*- scheme -*-
exec guile -e main -s "$0" "$@"
!#

;;; This file is part of Magic.
;;;
;;; Magic is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Magic is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Magic.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (ice-9 getopt-long))

(define script)

(define (commit args)
  (set! script (string-append "ssh -t " (car args) " '" (cadr args) "'"))
  (system script)
  (newline))

(define (push args)
  (set! script (string-append "ssh -t " (car args) " 'bash -s sudo' < " 
			      (cadr args)))
  (system script)
  (newline))

(define (version)
  (display "\
maggic (Make Auto GNU Guile Install Commit) 0.0.1
Copyright (C) 2016 Daniel Pimentel

License GPLv3+: GNU GPL 3 or later <http://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
"))

(define (help)
  (display "\
Usage: maggic [OPTION]... [CODE]...
Evaluate code with Maggic, interactively or from a script.

  -c     commit code
  -p     push file script
  -v     display this help and exit
  -h     display version information and exit

Report bugs to: d4n1@member.fsf.org
Maggic home page: <http://www.notabug.org/d4n1/maggic>
"))

(define (main args)
  (let* ((option-spec '((commit-option (single-char #\c) (value #f))
			(push-option (single-char #\p) (value #f))
			(version-option (single-char #\v) (value #f))
			(help-option (single-char #\h) (value #f))))
	 (options (getopt-long args option-spec))
	 (commit-option (option-ref options 'commit-option #f))
	 (push-option (option-ref options 'push-option #f))
	 (version-option (option-ref options 'version-option #f))
	 (help-option (option-ref options 'help-option #f)))
    (if (or commit-option push-option version-option help-option)
	(begin
	  (if commit-option (commit (cddr args)))
	  (if push-option (push (cddr args)))
	  (if version-option (version))
	  (if help-option (help)))
	(begin
	  (version)
	  (newline)))))
